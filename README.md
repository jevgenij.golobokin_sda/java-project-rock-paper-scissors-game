# Rock Paper Scissors Game

Small console game made with a Markov Chain as AI.

Markov Chain implementation based on this source: 
https://medium.com/@ssaurel/creating-a-rock-paper-scissors-game-in-java-with-a-markov-chain-for-the-ai-7672954fd7f6

#### Launch game
Run GameLauncher and enjoy a battle with AI :)