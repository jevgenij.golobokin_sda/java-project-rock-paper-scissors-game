package com.eugenegolobokin.rockpaperscissors;

import java.util.Random;

public class MarkovChain {

    private int[][] markovChain;
    private int rounds = 0;
    static final Random RANDOM = new Random();

    public void initializeMarkovChain() {
        int length = Item.values().length;
        this.markovChain = new int[length][length];

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                markovChain[i][j] = 0;
            }
        }
    }

    public void updateMarkovChain(Item previous, Item next) {
        markovChain[previous.ordinal()][next.ordinal()]++;
    }

    public Item nextChoice(Item previousUserChoice) {
        // Can't use Markov Chain on first move, getting random choice
        if (rounds < 1) {
            return Item.values()[RANDOM.nextInt(Item.values().length)];
        }

        int nextUserChoiceIndex = 0;

        for (int i = 0; i < Item.values().length; i++) {
            int previousUserChoiceIndex = previousUserChoice.ordinal();

            if (markovChain[previousUserChoiceIndex][i] > markovChain[previousUserChoiceIndex][nextUserChoiceIndex]) {
                nextUserChoiceIndex = i;
            }
        }

        Item nextPredictedUserChoice = Item.values()[nextUserChoiceIndex];

        return nextPredictedUserChoice.losesTo;
    }

    public void setRounds(int rounds) {
        this.rounds = rounds;
    }
}
