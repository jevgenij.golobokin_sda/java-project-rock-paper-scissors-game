package com.eugenegolobokin.rockpaperscissors;

import java.util.Random;
import java.util.Scanner;

public class GameEngine {

    static Scanner scanner = new Scanner(System.in);
    static final Random RANDOM = new Random();
    static MarkovChain markovChain = new MarkovChain();
    private Item last = null;

    public void launchGame() {

        markovChain.initializeMarkovChain();
        String userInput;
        int userScore = 0;
        int computerScore = 0;
        int matchCounter = 1;

        //  String[] computerSelectionsArray = {"rock", "paper", "scissors"};


        while (userScore < 5 && computerScore < 5) {

            System.out.println("Match " + matchCounter + "! Select your shape: Rock, Paper or Scissors?");
            userInput = scanner.nextLine().toUpperCase();

            // check if user input is valid
            Item userChoice;
            try {
                userChoice = Item.valueOf(userInput);
            } catch (Exception e) {
                System.out.println("Invalid choice!");
                continue;
            }

            Item computerChoice = markovChain.nextChoice(last);
            markovChain.setRounds(matchCounter);

            // update Markov Chain
            if (last != null) {
                markovChain.updateMarkovChain(last, userChoice);
            }
            last = userChoice;

            printChoices(userChoice, computerChoice);

            if (computerChoice.equals(userChoice)) {
                System.out.println("Draw!");
                printScore(userScore, computerScore);
                continue;
            } else if (computerChoice.losesTo(userChoice)) {
                userScore++;
                System.out.println("You win!");
                printScore(userScore, computerScore);
            } else {
                computerScore++;
                System.out.println("Computer wins!");
                printScore(userScore, computerScore);
            }

            matchCounter++;
        }

        getGameResults(userScore, computerScore);
    }

    public void printChoices(Item userChoice, Item computerChoice) {
        System.out.println("Your choice: " + userChoice + "\nComputer's choice: " + computerChoice);
    }

    public void printScore(int userScore, int computerScore) {
        System.out.println("Your score: " + userScore + "   Computer's score: " + computerScore);
        System.out.println();
    }

    public void getGameResults(int userScore, int computerScore) {

        if (userScore > computerScore) {
            System.out.println("Congratulations!! You win the game!");
            printScore(userScore, computerScore);
        } else {
            System.out.println("Oh noes! You lose! Hope you can beat this super smart computer next time..");
            printScore(userScore, computerScore);
            System.out.println("See you!!");
        }
    }
}

