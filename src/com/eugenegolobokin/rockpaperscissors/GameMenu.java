package com.eugenegolobokin.rockpaperscissors;

import java.util.Scanner;

public class GameMenu {

    public void launchMenu() {

        boolean gameIsRunning = true;
        System.out.println("Hi! This is Rock, Paper, Scissors Game! Can you beat a computer?");
        System.out.println("1 Yeah, I'm ready! \n2 Hold on a minute! What are the rules?");

        while (gameIsRunning) {
            Scanner scanner = new Scanner(System.in);
            String startOrPrintRules = scanner.nextLine();

            if (startOrPrintRules.equals("1")) {
                GameEngine gameEngine = new GameEngine();
                gameEngine.launchGame();
                gameIsRunning = false;
            }
            if (startOrPrintRules.equals("2")) {
                printRules();
            }
            if (!startOrPrintRules.equals("1") && !startOrPrintRules.equals("2")) {
                System.out.println("Invalid selection!");
                System.out.println("1 Start a game \n2 Read the rules");
            }
        }

    }

    public void printRules() {
        System.out.println("After you start a game, you and computer simultaneously select \n" +
                "one of three shapes:  \"rock\", \"paper\", or \"scissors\". \n" +
                "The winner is then selected based on standard game rules: \n" +
                "rock crushes scissors; paper covers rock; scissors cuts paper. \n" +
                "The winner gets one point, and if match ends in draw none of the players \n" +
                "gets a point. The game is played till one of players wins by collecting \n" +
                "5 points.");
        System.out.println();
        System.out.println("1 Great! Let's play! \n2 Wait, wait, I didn't get the rules!");
    }

}
