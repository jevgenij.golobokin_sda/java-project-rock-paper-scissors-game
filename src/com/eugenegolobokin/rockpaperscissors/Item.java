package com.eugenegolobokin.rockpaperscissors;

public enum Item {
    ROCK,
    PAPER,
    SCISSORS;

    public Item losesTo;

    public boolean losesTo(Item otherSelection) {
        return losesTo.equals(otherSelection);
    }

    static {
        ROCK.losesTo = PAPER;
        PAPER.losesTo = SCISSORS;
        SCISSORS.losesTo = ROCK;
    }

}
